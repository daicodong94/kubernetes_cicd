package tms.allnow

def defaultEnv(envName) {
    if (envName == "dev") {
        env.HARBOR_CRT = "gcr:all-now-tms-non-prod"    
    } else if (envName == "preprod"){
        env.HARBOR_CRT = "gcr:all-now-tms-staging"
    } else {
        env.HARBOR_CRT = "gcr:all-now-tms-$envName"
    }
    env.CREDENTIALSID = "Gitlab_crt_staging"
    env.HARBOR = 'https://asia.gcr.io'
    env.HARBOR_DOMAIN = "asia.gcr.io"
}

def mavenBuild(commands) {
    stage("Maven Build") {
        container("maven") {
            for (command in commands) {
                sh "$command"
            }
        }
    }
}

def mavenTest(commands) {
    def testSuccess = true
    stage("Maven Test") {
        container("maven") {
            for (command in commands) {
                try {
                    sh "$command"
                } catch (Exception e) {
                    testSuccess = false
                    break
                }
            }
        }
    }
    return testSuccess
}

def npmBuild(commands) {
    stage("npm Build") {
        sh 'rm -rf dist'
        container("node-builder") {
            for (command in commands) {
                sh "$command"
            }
        }
    }
}

def kanikoBuild(args) {
    def HARBOR_DOMAIN = args.HARBOR_DOMAIN
    def HARBOR_PROJECT = args.HARBOR_PROJECT
    def runenv = args.runenv
    def tag = args.tag
    def context = args.context
    if (runenv == "dev") {
        HARBOR_PROJECT = HARBOR_PROJECT.replace("dev", "non-prod")
    }
    if (runenv == "preprod") {
        HARBOR_PROJECT = HARBOR_PROJECT.replace("preprod", "staging")
    }

    container(name: "kaniko", shell: "/busybox/sh") {
        withEnv(['PATH+EXTRA=/busybox']) {
            sh """#!/busybox/sh -xe
                /kaniko/executor --context=${context} --destination=${HARBOR_DOMAIN}/${HARBOR_PROJECT}-${runenv}:${tag}
            """
        }
    }
}

def getTemplate(envName) {
    env.CREDENTIALSID = 'Gitlab_crt_staging'
    stage('Get DockerTemplate') {
        dir('template') {
            def GIT_URL_DOCKER_TEMPLATE = 'https://gitlab.com/tms-secrets/orchestration.git'
            def branch = 'main'

            git branch: "${branch}", credentialsId: "${CREDENTIALSID}", url: "${GIT_URL_DOCKER_TEMPLATE}"
            def templateDir = sh(returnStdout: true, script: 'echo $(pwd)').trim() + "/tms-$envName"
            return templateDir
        }
    }
}

def googleCloudAuth(){
    withCredentials([file(credentialsId: 'all-now-tms-staging-sa', variable: 'GOOGLE_SA')]) {
        sh "gcloud auth login --cred-file=\$GOOGLE_SA"
        sh "gcloud config set project all-now-tms-staging"
    }
}