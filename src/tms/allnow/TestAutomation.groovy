package tms.allnow

def run(args) {
    def envName = args.envName
    def specName = args.specName
    def runDate = args.runDate == null ? "None" : args.runDate
    def branchName = args.branchName == null ? "develop" : args.branchName

    stage("Running automation script, env: ${envName}, spec: ${specName}"){
        retry(3){
            build job: 'automation_frontend', parameters: [
                [$class: 'StringParameterValue', name: 'envName', value: "${envName}"],
                [$class: 'StringParameterValue', name: 'specName', value: "${specName}"],
                [$class: 'StringParameterValue', name: 'runDate', value: "${runDate}"],
                [$class: 'StringParameterValue', name: 'branchName', value: "${branchName}"]
            ]
        }
    }
}

def runSpec(args){
    def stagesContainer = []
    def specsList = args.specsList
    def envName = args.envName
    def runDate = args.runDate == null ? "" : args.runDate
    def branchName = args.branchName

    specsList.collate(8).each { specs ->
        def stagesTemp = [:]
        specs.each { spec ->
            stagesTemp.put(
                    "${envName}-$spec",
                    {
                        run(envName: envName, specName: spec, runDate : runDate, branchName: branchName)
                    }
            )
        }
        stagesContainer << stagesTemp
    }

    for(stages in stagesContainer) {
        parallel(stages)
    }
}