package tms.allnow

def triggerByJob(jobParams = [], allowConcurrentBuild = false){
    def propertiesList = []
    if (!allowConcurrentBuild) {
        propertiesList << disableConcurrentBuilds()
    }

    if (jobParams.size() != 0) {
        propertiesList << parameters(jobParams)
    }
    properties(propertiesList)
}

def timerTrigger(jobParams = [], cronTime, allowConcurrentBuild = false){
    def propertiesList = []
    propertiesList << pipelineTriggers([cron("${cronTime}")])

    if (jobParams.size() != 0) {
        propertiesList << parameters(jobParams)
    }

    if (!allowConcurrentBuild) {
        propertiesList << disableConcurrentBuilds()
    }

    properties(propertiesList)
}