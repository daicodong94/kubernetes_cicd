package tms.allnow

def deploy(args){
    def serviceName = args.serviceName
    def imageName = args.imageName
    def baseDIR = args.baseDIR
    def dockerTag = args.dockerTag
    def envName = args.envName
    def registryDocker = args.registryDocker
    def projectID = args.projectID
    if (envName == "dev"){
        projectID = "all-now-tms-non-prod"
    }
    if (envName == "preprod"){
        projectID = "all-now-tms-staging"
    }

    def GIT_URL = 'https://gitlab.com/tms-secrets/gitops.git'
    def DIR = "tms-${envName}"
    def branch = "main"

    def usersNotAllowToDeploy = ["jenkins-automation"]

    stage('Preparing ArgoCD Repository') {

        wrap([$class: 'BuildUser']) {
            def user = env.BUILD_USER_ID
            if(user in usersNotAllowToDeploy){
                error("This user name: ${user} don't have permission to deploy.")
                return
            }
        }

        retry(10) {
            sh "rm -rf gitops"
            dir("gitops"){
                git  branch: "${branch}", credentialsId: "Gitlab_crt_staging", url: "${GIT_URL}"
                sh "sed -i 's/image: .*/image: ${registryDocker}\\/${projectID}\\/${baseDIR}\\/${imageName}:${dockerTag}/g' ${DIR}/${serviceName}/${serviceName}.yml"

                withCredentials([gitUsernamePassword(credentialsId: "Gitlab_crt_staging", gitToolName: 'git-tool')]) {
                    sh "git config --global user.email 'commerce-techops@ascendcorp.com'"
                    sh "git config --global user.name 'devops'"
                    sh "git add  ${DIR}/${serviceName}/${serviceName}.yml"
                    sh "git commit -m '(pipeline)change image to $dockerTag' "
                    sh "git push origin HEAD:$branch --force"
                }
            }
        }
    }
}
