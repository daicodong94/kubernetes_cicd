package tms.allnow

import com.cloudbees.groovy.cps.NonCPS

class PodTemplates {

  String yaml
  def projectName
  def envName

  PodTemplates(projectName = "all-now-tms-staging", envName = "staging") {
    this.projectName = projectName
    this.envName = envName
    this.yaml = """
---
apiVersion: v1
kind: Pod
spec:
  volumes:
  - name: kaniko-secret
    secret:
      secretName: docker-jenkins-${envName}
      items:
      - key: .dockerconfigjson
        path: config.json
  nodeSelector:
    testnode: "true"      
  tolerations:
  - key: "test_node"
    operator: "Equal"
    value: "true"
    effect: "NoSchedule"   
  hostIPC: true
  containers:
  - name: jnlp
    image: jenkins/inbound-agent:4.3-4
    resources:
      requests:
        cpu: 500m
        memory: 500Mi     
"""
  }

  PodTemplates addKaniko() {
    this.yaml += """
  - name: kaniko
    image: gcr.io/kaniko-project/executor:v1.5.1-debug
    tty: true
    command:
    - /busybox/cat
    resources:
      requests:
        cpu: 100m
        memory: 500Mi
    volumeMounts:
    - name: kaniko-secret
      mountPath: /kaniko/.docker
"""
    return this
  }

  PodTemplates addMaven(tag="latest") {
    this.yaml += """
  - name: maven
    image: asia.gcr.io/${projectName}/devops-tool/maven/staging:${tag}
    command:
    - cat
    tty: true
    resources:
      requests:
        cpu: 200m
        memory: 500Mi
"""
    return this
  }

  PodTemplates addNodeBuilder(tag="16.15.1") {
    this.yaml += """
  - name: node-builder
    image: node:${tag}
    command:
    - cat
    tty: true
    resources:
      requests:
        cpu: 200m
        memory: 1000Mi
"""
    return this
  }

  PodTemplates addCypressBuilder(tag="0.0.2") {
    this.yaml += """
  - name: cypress-builder
    image: asia.gcr.io/all-now-tms-staging/devops-tool/cypress:${tag}
    tty: true
    command:
    - /bin/bash
    securityContext:
      runAsUser: 1000
    resources:
      requests:
        cpu: 1000m
        memory: 4000Mi
"""
    return this
  }

  PodTemplates addCypressBuilderNew(tag="0.0.2") {
    this.yaml += """
  - name: cypress-builder2
    image: cypress/browsers:node18.12.0-chrome107
    tty: true
    command:
    - /bin/bash
    securityContext:
      runAsUser: 1000
    resources:
      requests:
        cpu: 1000m
        memory: 4000Mi
"""
    return this
  }

  PodTemplates addCloudsdk(tag="alpine") {
    this.yaml += """
  - name: cloud-sdk
    image: asia.gcr.io/all-now-tms-staging/devops-tool/cloud-sdk:${tag}
    tty: true
    command:
    - /bin/bash
    securityContext:
      runAsUser: 1000
    resources:
      requests:
        cpu: 200m
        memory: 500Mi
"""
    return this
  }

  PodTemplates addUbuntu(tag="18.04") {
    this.yaml += """
  - name: ubuntu
    image: asia.gcr.io/all-now-tms-staging/devops-tool/ubuntu:${tag}
    tty: true
    command:
    - /bin/bash
    securityContext:
      runAsUser: 0
    resources:
      requests:
        cpu: 2000m
        memory: 4000Mi
"""
    return this
  }

  PodTemplates addRobot(tag="v5.3") {
    this.yaml += """
  - name: robot-tests
    image: asia.gcr.io/all-now-tms-staging/devops-tool/robot-image:${tag}
    tty: true
    command:
    - /bin/bash
    securityContext:
      runAsUser: 0
    resources:
      requests:
        cpu: 500m
        memory: 1000Mi
"""
    return this
  }

  @NonCPS
  String toString() {
    return this.yaml
  }

}
