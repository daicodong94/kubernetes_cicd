import tms.allnow.*

def call() {
    def gitlabRepoName = env.GITLAB_REPO_NAME
    def gitlabRepoRef = env.GITLAB_REPO_GIT_HTTP_URL
    def branchName = env.BRANCH_NAME
    def envName = branchName.split("-")[0]
    def hashValue = env.GITLAB_CHECKOUT_SHA
    def CREDENTIALSID = "Gitlab_crt_staging"
    def pipeline = new Pipeline()
    pipeline.triggerByJob([], true)

    if (!(branchName in ["dev-deploy", "staging-deploy", "prod-deploy"])) {
        println "This branch $branchName not allow to execute Jenkins CI/CD!"
        return
    }

    PodTemplates pt = new PodTemplates("all-now-tms-staging")
    podTemplate(yaml: pt.toString()) {
        node(POD_LABEL) {
            def serviceName
            def jobName
            def actions

            stage("Getting job information") {
                dir(gitlabRepoName) {
                    git(branch: branchName, credentialsId: CREDENTIALSID, url: gitlabRepoRef)
                    serviceName = sh(script: "git log --pretty=oneline ${hashValue} | awk '\$0~var {print \$2}' var=\"${hashValue}\"", returnStdout: true).toString().trim()

                    def buildInformation = serviceName.split("_")

                    jobName = buildInformation[1]
                    actions = buildInformation[2]
                }
            }

            stage("Running job $jobName for $actions") {
                if (serviceName.contains("jenkins-builder")) {
                    def parameters = [
                            [$class: 'StringParameterValue', name: 'runenv', value: envName],
                            [$class: 'StringParameterValue', name: 'branch', value: branchName],
                            [$class: 'StringParameterValue', name: 'tag', value: "latest"]
                    ]

                    switch (actions) {
                        case "build-deploy":
                            parameters << [$class: 'BooleanParameterValue', name: 'allowDeploy', value: "true"]
                            break
                        case "build":
                            parameters << [$class: 'BooleanParameterValue', name: 'allowDeploy', value: "false"]
                            break
                        default:
                            return
                    }

                    build job: jobName, parameters: parameters
                } else {
                    println("Not execute CI/CD pipeline!")
                }
            }
        }
    }
}
