import tms.allnow.*
import java.text.SimpleDateFormat

def call() {
    TimeZone.setDefault(TimeZone.getTimeZone('Asia/Bangkok'))

    def pipeline = new Pipeline()
    def global = new Global()
    def jobParams = [
            choice(choices: ['None', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '17', '18', '19', '20', '21'], name: 'SCENARIO', description: 'Choose target scenario.'),
            string(defaultValue: '0', name: 'USER_QUANTITY',
                    description: """|Number of Threads(users)
                                    |- Max for Scenario 9 and 10 is 840
                                    |- No limit for Scenario 1, 2, 3, 4, 5, 6, 7, 8, 17, 18, 19, 20 and 21""".stripMargin()),
            string(defaultValue: '0', name: 'RAMP_UP', description: 'Ramp-up period (seconds)'),
            string(defaultValue: 'none@ascendcorp.com', name: 'REPORT_EMAIL', description: 'report email (Optional)')
    ]
    def jmeterScriptBranch = "main"
    def CREDENTIALSID = "Gitlab_crt_staging"
    def GIT_URL = "https://gitlab.com/asc-jiuye/automate/tmsbrain_performancetest.git"

    def SCENARIO = params.SCENARIO
    def USER_QUANTITY = params.USER_QUANTITY
    def RAMP_UP = params.RAMP_UP
    def REPORT_EMAIL = params.REPORT_EMAIL

    if (SCENARIO == 'None' || USER_QUANTITY == "0" || RAMP_UP == "0") {
        error("Wrong input parametes. Please check !")
    }

    if ((SCENARIO == "9" || SCENARIO == "10") && USER_QUANTITY.toInteger() > 840) {
        error("Number of Users for Scenario ${SCENARIO} exceed the limit is 840")
    }

    def date = new Date()
    def day = new SimpleDateFormat("dd-MM-yyyy").format(date)
    pipeline.triggerByJob(jobParams, true)

    PodTemplates pt = new PodTemplates("all-now-tms-staging", "staging").addUbuntu("18.04-0.0.3").addCloudsdk()
    podTemplate(yaml: pt.toString()) {
        node(POD_LABEL) {
            def RESULTS_FOLDER
            def REPORTS_FOLDER
            def JMETER_CLI
            def WORK_PROJECT

            stage("Downloading jmeter scripts") {
                container("ubuntu") {
                    JMETER_CLI = sh(script: "env | grep -i JMETER_CLI", returnStdout: true).trim().split("=")[1].replace("/", "\\/")
                    WORK_PROJECT = sh(script: "echo \$(pwd)/tmsbrain_performancetest", returnStdout: true).trim().toString().replace("/", "\\/")
                    RESULTS_FOLDER = "${WORK_PROJECT}/results"
                    REPORTS_FOLDER = "${WORK_PROJECT}/reports"

                    dir("tmsbrain_performancetest") {
                        git branch: jmeterScriptBranch, credentialsId: CREDENTIALSID, url: GIT_URL
                    }
                }
            }

            stage("Running automation load test") {
                container("ubuntu") {
                    dir("tmsbrain_performancetest") {
                        sh "sed -i 's/JMETER_CLI=<JMETER_CLI>/JMETER_CLI=${JMETER_CLI}/g' run.sh"
                        sh "sed -i 's/WORK_PROJECT=<WORK_PROJECT>/WORK_PROJECT=${WORK_PROJECT}/g' run.sh"
                        sh "sed -i 's/SCENARIO=<SCENARIO>/SCENARIO=${SCENARIO}/g' run.sh"
                        sh "sed -i 's/USER_QUANTITY=<USER_QUANTITY>/USER_QUANTITY=${USER_QUANTITY}/g' run.sh"
                        sh "sed -i 's/RAMP_UP=<RAMP_UP>/RAMP_UP=${RAMP_UP}/g' run.sh"
                        sh "sed -i 's/REPORT_EMAIL=<REPORT_EMAIL>/REPORT_EMAIL=${REPORT_EMAIL}/g' run.sh"
                        sh "chmod +x run.sh"
                        sh "./run.sh"
                        sh "cd reports/scenario-$SCENARIO; zip -r ${USER_QUANTITY}-${RAMP_UP}.zip ${USER_QUANTITY}-${RAMP_UP}"
                    }
                }
            }

            stage("Uploading results") {
                sh "cp ${REPORTS_FOLDER}/scenario-$SCENARIO/${USER_QUANTITY}-${RAMP_UP}.zip ."
                sh "cp $RESULTS_FOLDER/scenario-$SCENARIO/$USER_QUANTITY-${RAMP_UP}.csv ."
                sh "cp $RESULTS_FOLDER/scenario-$SCENARIO/$USER_QUANTITY-${RAMP_UP}-errors.xml ."

                container("cloud-sdk") {
                    global.googleCloudAuth()
                    def googleStorageDestinatioin = "gs://automation-result/tmsbrain_performancetest/$day/scenario_$SCENARIO"

//                    Upload report folder
                    sh "gsutil cp ${USER_QUANTITY}-${RAMP_UP}.zip $googleStorageDestinatioin/reports/"
                    sh "gsutil cp $USER_QUANTITY-${RAMP_UP}.csv $googleStorageDestinatioin/results/"
                    sh "gsutil cp $USER_QUANTITY-${RAMP_UP}-errors.xml $googleStorageDestinatioin/results/"
                }
            }

            stage("Send google chat notification") {
                googlechatnotification(
                        url: "https://chat.googleapis.com/v1/spaces/AAAAyBL5iPc/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=iiBA1479xJqFQ22XiIR8xwGsbRGmdXbQnvf9NGmsbTw%3D",
                        message: """|Performance test has finished
                                    |Run date: $day
                                    |Scenario: $SCENARIO
                                    |THREAD_NUMBER = $USER_QUANTITY
                                    |RAMP_UP = $RAMP_UP
                                    |Check the results at:
                                    |https://console.cloud.google.com/storage/browser/automation-result/tmsbrain_performancetest/$day/scenario_$SCENARIO?project=all-now-tms-staging""".stripMargin(),
                        sameThreadNotification: false
                )
            }
        }
    }
}

