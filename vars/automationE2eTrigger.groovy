import tms.allnow.*
import java.text.SimpleDateFormat

def call() {
    TimeZone.setDefault(TimeZone.getTimeZone('Asia/Bangkok'))

    def pipeline = new Pipeline()

    pipeline.timerTrigger([], 'TZ=Asia/Bangkok\n30 8,22 * * 1,2,3,4,5')
//    def envNames = ["staging","production"]
    def envNames = ["staging"]

    PodTemplates pt = new PodTemplates("all-now-tms-staging")
    podTemplate(yaml: pt.toString()) {
        node(POD_LABEL) {
            def stages = [:]
            envNames.each { envName ->
                stages.put(
                        envName,
                        {
                            build job: 'e2e-automation-frontend', parameters: [
                                    [$class: 'StringParameterValue', name: 'envName', value: "${envName}"]
                            ]
                        }
                )
            }
            parallel(stages)
        }
    }
}
