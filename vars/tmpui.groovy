import tms.allnow.*

def call() {
    def global = new Global()
    def pipeline = new Pipeline()
    def jobParams = [
            choice(choices: ['dev', 'staging', 'preprod', 'prod'], name: 'runenv', description: 'Target environments'),
            string(defaultValue: 'master', name: 'branch', description: 'Target branch'),
            string(defaultValue: 'latest', name: 'tag', description: 'Image tag'),
            booleanParam(name: 'allowDeploy', description: 'Kubernetes auto deploy')
    ]

    pipeline.triggerByJob(jobParams)
    def nodeVersion = "${runenv}" == "staging" ? "14.17.0" : "12.15.0"

    PodTemplates pt = new PodTemplates("all-now-tms-staging", "${runenv}").addNodeBuilder(nodeVersion).addKaniko()
    podTemplate(yaml: pt.toString()) {
        env.GIT_URL = 'https://gitlab.com/asc-jiuye/frontend/tmpui.git'
        env.HARBOR_PROJECT = "all-now-tms-${runenv}/tms/tmpui"

        global.defaultEnv("${runenv}")

        node(POD_LABEL) {
            def tag = sh(returnStdout: true, script: "echo ${params.tag}-${env.BUILD_NUMBER}").trim()

            stage("Pulling ${JOB_NAME} repository") {
                echo "mirror name: ${HARBOR_PROJECT}-${runenv}:${tag}"
                git branch: "${branch}", credentialsId: "${CREDENTIALSID}", url: "${GIT_URL}"
            }

            global.npmBuild([
                    'npm config get registry',
                    'npm install --legacy-peer-deps && npm run prod'
            ])

            stage('Build Image and Push') {
                global.kanikoBuild(
                        HARBOR_DOMAIN: "$HARBOR_DOMAIN",
                        HARBOR_PROJECT: "$HARBOR_PROJECT",
                        runenv: "$runenv",
                        tag: tag,
                        context: "."
                )
            }

            if (params.allowDeploy) {
                def k8s = new K8s()
                k8s.deploy(
                        serviceName: "${JOB_NAME}",
                        imageName: "${JOB_NAME}-${runenv}",
                        baseDIR: "tms",
                        dockerTag: "${tag}",
                        envName: "${runenv}",
                        registryDocker: "asia.gcr.io",
                        projectID: "all-now-tms-${runenv}"
                )
            }
        }
    }
}
