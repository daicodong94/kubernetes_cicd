import tms.allnow.*

def call() {
    TimeZone.setDefault(TimeZone.getTimeZone('Asia/Bangkok'))

    def pipeline = new Pipeline()
    pipeline.timerTrigger([], 'TZ=Asia/Bangkok\n0 6 * * 1,2,3,4,5')

    PodTemplates pt = new PodTemplates("all-now-tms-staging")
    podTemplate(yaml: pt.toString()) {
        node(POD_LABEL) {
            stage("Set replica") {
                def tmsServices = [
                        "tmsui", "tai-main", "mdt-main", "gmc-main", "tms2-job",
                        "imc-rest", "tmpui", "tenant-main", "imc-main", "tmc-main",
                        "lmc-main", "lbs-main", "supplier", "message-main", "account-site",
                        "bmc-main", "cp-tms-rest-center", "message-job", "lmc-collector", "tms2-main",
                        "framework-service-main", "account-main", "billing-main", "rc-main", "tms2-rest"
                ]

                dir("gitops") {
                    def defaultReplicaNumb = [:]
                    def environments = ["tms-dev", "tms-staging"]

                    git branch: "main", credentialsId: "Gitlab_crt_staging", url: "https://gitlab.com/tms-secrets/gitops.git"

                    environments.each { envName ->
                        def tmpReplicaNumb = [:]

                        tmsServices.each { service ->
                            dir("${envName}/$service") {
                                def replicaInfo = sh(script: "cat ${service}.yml | grep replicas", returnStdout: true).trim().tokenize(":")
                                def replicaVal = replicaInfo.last().toString().trim()

                                tmpReplicaNumb[service] = replicaVal
                                sh "sed -i \"s/replicas: .*/replicas: 0/g\" ${service}.yml"
                                sh "cat ${service}.yml"
                            }
                        }

                        defaultReplicaNumb[envName] = tmpReplicaNumb
                    }
                    commitChanges()

                    // Wait ArgoCD take actions
                    sleep(time: "10", unit: "MINUTES")

                    environments.each { envName ->
                        tmsServices.each { service ->
                            dir("${envName}/$service") {
                                sh "sed -i \"s/replicas: .*/replicas: ${defaultReplicaNumb[envName]["${service}"]}/g\" ${service}.yml"
                                sh "cat ${service}.yml"
                            }
                        }
                    }

                    commitChanges()
                }
            }
        }
    }
}

def commitChanges() {
    withCredentials([gitUsernamePassword(credentialsId: "Gitlab_crt_staging", gitToolName: 'git-tool')]) {
        sh "git config --global user.email 'commerce-techops@ascendcorp.com'"
        sh "git config --global user.name 'devops'"
        retry(10) {
            sh "git add  .; git commit -m 'Jenkins set replica'; git push origin HEAD:main --force"
        }
    }
}