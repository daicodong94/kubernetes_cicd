import tms.allnow.*

def call() {
    def global = new Global()
    def pipeline = new Pipeline()
    def jobParams = [
            choice(choices: ['dev', 'staging', 'preprod', 'prod'], name: 'runenv', description: 'Target environments'),
            string(defaultValue: 'master', name: 'branch', description: 'Target branch')
    ]

    pipeline.triggerByJob(jobParams)

    PodTemplates pt = new PodTemplates("all-now-tms-staging", "${runenv}").addMaven()
    podTemplate(yaml: pt.toString()) {
        env.GIT_URL = 'https://gitlab.com/asc-jiuye/backend/bmc.git'
        global.defaultEnv("${runenv}")

        node(POD_LABEL) {
            stage("Pulling ${JOB_NAME} repository") {
                git branch: "${branch}", credentialsId: "${CREDENTIALSID}", url: "${GIT_URL}"

                sh "sed -i '/<module>bmc-main<\\/module>/d' pom.xml"
                sh "sed -i '/<module>bmc-service<\\/module>/d' pom.xml"
            }

            global.mavenBuild([
                    'mvn clean deploy -Dmaven.test.skip=true'
            ])
        }
    }
}
