import tms.allnow.*
import java.text.SimpleDateFormat

def call() {
    def global = new Global()
    def pipeline = new Pipeline()
    def jobParams = [
            choice(choices: ['dev', 'staging', 'preprod', 'prod'], name: 'runenv', description: 'Target environments'),
            string(defaultValue: 'master', name: 'branch', description: 'Target branch'),
            string(defaultValue: 'latest', name: 'tag', description: 'Image tag'),
            booleanParam(name: 'allowDeploy', description: 'Kubernetes auto deploy')
    ]

    pipeline.triggerByJob(jobParams)

    PodTemplates pt = new PodTemplates("all-now-tms-staging", "${runenv}").addMaven().addKaniko()
    podTemplate(yaml: pt.toString()) {
        env.GIT_URL = 'https://gitlab.com/asc-jiuye/backend/tmc.git'
        env.HARBOR_PROJECT = "all-now-tms-${runenv}/tms/tmc-main"
        env.DIR = 'tmc-main'
        
        global.defaultEnv("${runenv}")

        node(POD_LABEL) {
            def tag = sh(returnStdout: true, script: "echo ${params.tag}-${env.BUILD_NUMBER}").trim()
            def date = new Date()
            def day = new SimpleDateFormat("dd-MM-yyyy").format(date)

            stage("Pulling ${JOB_NAME} repository") {
                echo "mirror name: ${HARBOR_PROJECT}-${runenv}:${tag}"
                git branch: "${branch}", credentialsId: "${CREDENTIALSID}", url: "${GIT_URL}"
            }

            
            global.mavenBuild([
                'mvn -U clean package -Dmaven.test.skip=true'
            ])

            boolean isTestSuccessful = global.mavenTest([ 'mvn test -U' ])

            junit testResults: '**/target/surefire-reports/TEST-*.xml', allowEmptyResults: true

            // sleep(time: "10", unit: "HOURS")

            if (isTestSuccessful){
                
                httpRequest(
                    url: 'https://discord.com/api/webhooks/1090843004958416947/v-7V9s-g1d01BznJDQMYkxnOkIPUvaCC-IXG5RV-_5bRKLqQhldn6C2vXBOVTcf3vbcL',
                    httpMode: 'POST',
                    contentType: 'APPLICATION_JSON',
                    requestBody: """{
                        "content": "tmc-main unit test has success.\\nRun date: $day\\nCheck the results at:\\nhttps://jenkins-stg.allnowgroup.com/job/tmc-main/${BUILD_NUMBER}/console"
                    }"""
                )

            } else {

                httpRequest(
                    url: 'https://discord.com/api/webhooks/1090843004958416947/v-7V9s-g1d01BznJDQMYkxnOkIPUvaCC-IXG5RV-_5bRKLqQhldn6C2vXBOVTcf3vbcL',
                    httpMode: 'POST',
                    contentType: 'APPLICATION_JSON',
                    requestBody: """{
                        "content": "tmc-main unit test has failed.\\nRun date: $day\\nCheck the results at:\\nhttps://jenkins-stg.allnowgroup.com/job/tmc-main/${BUILD_NUMBER}/console"
                    }"""
                )

                error("tmc-main unit test has failed.")
            }


            def templateDir = global.getTemplate("${runenv}")

            stage('Build Image and Push') {
                dir("${DIR}") {
                    EXPOSE_LINE = sh(returnStdout: true, script: "awk '/EXPOSE/{print}' Dockerfile").trim()
                    sh "cp ${templateDir}/DockerTemplates/DubboDockerfile Dockerfile"
                    sh "cp ${templateDir}/DockerTemplates/entrypoint.sh ."
                    sh "sed -i 's/#{EXPOSE}/${EXPOSE_LINE}/g' Dockerfile"
                    sh "sed -i 's/#{SERVICES}/${DIR}/g' Dockerfile"

                    global.kanikoBuild(
                            HARBOR_DOMAIN: "$HARBOR_DOMAIN",
                            HARBOR_PROJECT: "$HARBOR_PROJECT",
                            runenv: "$runenv",
                            tag: tag,
                            context: "."
                    )
                }
            }

            if (params.allowDeploy) {
                def k8s = new K8s()
                k8s.deploy(
                        serviceName: "${JOB_NAME}",
                        imageName: "${JOB_NAME}-${runenv}",
                        baseDIR: "tms",
                        dockerTag: "${tag}",
                        envName: "${runenv}",
                        registryDocker: "asia.gcr.io",
                        projectID: "all-now-tms-${runenv}"
                )
            }
        }
    }
}
