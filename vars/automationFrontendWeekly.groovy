import tms.allnow.*
import java.text.SimpleDateFormat

def call() {
    TimeZone.setDefault(TimeZone.getTimeZone('Asia/Bangkok'))

    properties([
        disableConcurrentBuilds(),
        pipelineTriggers([cron('TZ=Asia/Bangkok\nH 9 * * 5')])
    ])

    def testAutomation = new TestAutomation()

    PodTemplates pt = new PodTemplates("all-now-tms-staging")
    podTemplate(yaml: pt.toString()) {
        node(POD_LABEL) {
            def specsList
            def date = new Date()
            def day = new SimpleDateFormat("dd-MM-yyyy").format(date)

            stage("Get specs list") {
                dir("tmsbrain_automationfe") {
                    git branch: "develop", credentialsId: "Gitlab_crt_staging", url: "https://gitlab.com/asc-jiuye/automate/tmsbrain_automationfe.git"

                    dir("cypress/e2e/specs") {
                        specsList = sh(script: "find * -type f", returnStdout: true).tokenize("\n")
                    }
                }
            }

            stage("Staging automation") {
                testAutomation.runSpec(specsList : specsList, branchName: "staging", runDate: day, envName: "staging")
            }

//            stage("Production automation") {
//                testAutomation.runSpec(specsList, "production", day)
//            }

            stage("Send google chat notification") {
                googlechatnotification(
                        url: "https://chat.googleapis.com/v1/spaces/AAAAyBL5iPc/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=iiBA1479xJqFQ22XiIR8xwGsbRGmdXbQnvf9NGmsbTw%3D",
                        message: """|Automation test has finished
                                    |Run date: $day
                                    |Check the results at:
                                    |https://console.cloud.google.com/storage/browser/automation-result;tab=objects?project=all-now-tms-staging&prefix=&forceOnObjectsSortingFiltering=false""".stripMargin(),
                        sameThreadNotification: false
                )
            }
        }
    }
}
