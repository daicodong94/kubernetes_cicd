import tms.allnow.*
import java.text.SimpleDateFormat

def call() {
    TimeZone.setDefault(TimeZone.getTimeZone('Asia/Bangkok'))

    def pipeline = new Pipeline()
    def jobParams = [
            string(defaultValue: 'None', name: 'envName', trim: true, description: 'dev or staging'),
            string(defaultValue: 'web/regression', name: 'fileName', trim: true, description: 'please specific file path/name.robot'),
            choice(choices: ['en', 'th'], name: 'LANG', description: 'for specific language on test'),
            string(defaultValue: 'web', name: 'include', trim: true, description: 'for include test by tag eg.  web = -i web'),
            string(defaultValue: 'main', name: 'branch', description: 'Target branch')
    ]

    pipeline.triggerByJob(jobParams, true)
    def envName = params.envName
    def fileName = params.fileName

    if (envName == "None" || fileName == "None") {
        error("Please select environment and file path/name.robot")
        return
    }

    def gitCredential = "Gitlab_crt_staging"

    PodTemplates pt = new PodTemplates("all-now-tms-staging", "staging").addRobot()

    podTemplate(yaml: pt.toString()) {
        node(POD_LABEL) {
            dir("tmsb_automation_robotframework") {
                
                def workspace = pwd()
                def OUTPUT_DIR = "${workspace}/result"

                stage("Pull test repository") {
                    git branch: "${branch}", credentialsId: "Gitlab_crt_staging", url: "https://gitlab.com/asc-jiuye/automate/tmsb_automation_robotframework.git"
                }

                try {   
                    stage("executing robot framework script") {
                        container("robot-tests") {
                            sh "robot -d result -v env:${envName} -v LANG:${LANG} -i ${include} testcases/${fileName}"
                        }
                    }
                } finally {
                    stage('Publish Report') {
                        echo "Publish Robot Framework test results"
                        robot logFileName: 'log.html',
                              outputFileName: 'output.xml',
                              reportFileName: 'report.html',
                              otherFiles: '*.png',
                              outputPath: "${OUTPUT_DIR}"
                    }
                }   
            }
        }
    }
}
