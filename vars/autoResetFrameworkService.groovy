import tms.allnow.*

def call() {
    TimeZone.setDefault(TimeZone.getTimeZone('Asia/Bangkok'))

    def pipeline = new Pipeline()
    pipeline.timerTrigger([], 'TZ=Asia/Bangkok\n20 6 * * 1,2,3,4,5')

    PodTemplates pt = new PodTemplates("all-now-tms-staging").addCloudsdk("0.0.1")
    podTemplate(yaml: pt.toString()) {
        node(POD_LABEL) {
            stage("Restarting framework-service") {
                container("cloud-sdk") {
                    withCredentials([file(credentialsId: 'all-now-tms-staging-sa', variable: 'GOOGLE_SA')]) {
                        sh "gcloud auth login --cred-file=\$GOOGLE_SA"
                        sh "gcloud container clusters get-credentials tms-staging-cluster --region asia-southeast1 --project all-now-tms-staging"

                        def framworkServicePods = sh(script: "kubectl get pods --no-headers -o custom-columns=\":metadata.name\" -n tms-services | grep framework-service", returnStdout: true).trim().tokenize("\n")

                        framworkServicePods.each { framworkServicePod ->
//                            sh "kubectl delete pod $framworkServicePod -n tms-services"
                            sh "kubectl get all"
                        }
                    }
                }
            }
        }
    }
}
