import tms.allnow.*

def call() {
    def global = new Global()
    def pipeline = new Pipeline()
    def jobParams = [
            choice(choices: ['dev', 'staging', 'preprod', 'prod'], name: 'runenv', description: 'Target environments'),
            string(defaultValue: 'master', name: 'branch', description: 'Target branch'),
            string(defaultValue: 'latest', name: 'tag', description: 'Image tag'),
            booleanParam(name: 'allowDeploy', description: 'Kubernetes auto deploy')
    ]

    pipeline.triggerByJob(jobParams)

    PodTemplates pt = new PodTemplates("all-now-tms-staging", "${runenv}").addMaven().addKaniko()
    podTemplate(yaml: pt.toString()) {
        env.GIT_URL = 'https://gitlab.com/asc-jiuye/backend/imc.git'
        env.HARBOR_PROJECT = "all-now-tms-${runenv}/tms/imc-rest"
        env.DIR = 'imc-rest'

        global.defaultEnv("${runenv}")

        node(POD_LABEL) {
            def tag = sh(returnStdout: true, script: "echo ${params.tag}-${env.BUILD_NUMBER}").trim()

            stage("Pulling ${JOB_NAME} repository") {
                echo "mirror name: ${HARBOR_PROJECT}-${runenv}:${tag}"
                git branch: "${branch}", credentialsId: "${CREDENTIALSID}", url: "${GIT_URL}"

                sh "sed -i '/<module>imc-service<\\/module>/d' pom.xml"
                sh "sed -i '/<module>imc-main<\\/module>/d' pom.xml"
            }

            global.mavenBuild([
                    'mvn clean package -Dmaven.test.skip=true'
            ])

            def templateDir = global.getTemplate("${runenv}")

            stage('Build Image and Push') {
                dir("${DIR}") {
                    if ("${runenv}" == "dev") {
                        sh "cp ${templateDir}/DockerTemplates/TomcatDockerfileNewrelic Dockerfile"
                        sh "sed -i 's/NEW_RELIC_APP_NAME=\"\"/NEW_RELIC_APP_NAME=\"${JOB_NAME}-${runenv}\"/g' Dockerfile"
                        sh "sed -i 's/NEW_RELIC_LICENSE_KEY=\"\"/NEW_RELIC_LICENSE_KEY=\"52824b069cb1f0059d3c08570f94ef3d18feNRAL\"/g' Dockerfile"
                    } else {
                        sh "cp ${templateDir}/DockerTemplates/TomcatDockerfile Dockerfile"
                    }
                    sh "sed -i 's/#{SERVICES}/${DIR}/g' Dockerfile"

                    global.kanikoBuild(
                            HARBOR_DOMAIN: "$HARBOR_DOMAIN",
                            HARBOR_PROJECT: "$HARBOR_PROJECT",
                            runenv: "$runenv",
                            tag: tag,
                            context: "."
                    )
                }
            }

            if (params.allowDeploy) {
                def k8s = new K8s()
                k8s.deploy(
                        serviceName: "${JOB_NAME}",
                        imageName: "${JOB_NAME}-${runenv}",
                        baseDIR: "tms",
                        dockerTag: "${tag}",
                        envName: "${runenv}",
                        registryDocker: "asia.gcr.io",
                        projectID: "all-now-tms-${runenv}"
                )
            }
        }
    }
}
