import tms.allnow.*
import java.text.SimpleDateFormat

def call() {
    TimeZone.setDefault(TimeZone.getTimeZone('Asia/Bangkok'))

    def scriptRepo = "https://gitlab.com/asc-jiuye/automate/tmsbrain_automationfe.git"
    def pipeline = new Pipeline()
    def jobParams = [
            string(defaultValue: 'None', name: 'envName', trim: true, description: 'staging or production'),
            string(defaultValue: 'None', name: 'specName', trim: true, description: 'Target Spec, example: TMP/*.js'),
            string(defaultValue: 'None', name: 'runDate', trim: true, description: 'Run date (Optional). Format: dd-MM-yyyy'),
            string(defaultValue: 'develop', name: 'branchName', trim: true, description: 'default: develop')
    ]
    pipeline.triggerByJob(jobParams, true)
    def envName = params.envName
    def specName = params.specName
    def runDate = params.runDate
    def branchName = params.branchName
    if (envName == "None" || specName == "None") {
        error("Please select environment and spec")
        return
    }

    if (runDate == "None") {
        def dateTime = new Date()
        def todayTime = new SimpleDateFormat("dd-MM-yyyy").format(dateTime)
        runDate = todayTime
    }

    def gitCredential = "Gitlab_crt_staging"

    PodTemplates pt = new PodTemplates("all-now-tms-staging", "staging").addCypressBuilder().addCloudsdk()

    podTemplate(yaml: pt.toString()) {
        node(POD_LABEL) {
            def resultPath = ""
            def testStatus = ""
            def videosFolderPath = ""

            dir("tmsbrain_automationfe") {
                stage("Pulling automation script") {
                    git branch: "${branchName}", credentialsId: gitCredential, url: scriptRepo
                }

                stage("NPM install modules") {
                    container("cypress-builder") {
                        sh "rm -rf node_modules package-lock.json"
                        sh "npm install"
                        sh "npm install cypress mochawesome-report-generator cy-verify-downloads"
                    }
                }

                stage("Running test scripts") {
                    container("cypress-builder") {
                        try {
                            sh "npx cypress run --headless --browser chrome --reporter cypress-mochawesome-reporter --env ENV=\"${envName}\" --spec \"cypress/e2e/specs/${specName}\""
                            testStatus = "passed"
                        } catch (e) {
                            println("Test failed !" + e)
                            testStatus = "failed"
                        }

                        dir("cypress/reports") {
                            resultPath = sh(script: "echo \$(pwd)", returnStdout: true).trim().toString() + "/index.html"
                        }

                        dir("cypress/videos") {
                            videosFolderPath = sh(script: "echo \$(pwd)", returnStdout: true).trim().toString()
                        }
                    }
                }
            }

            stage("Update result") {
                def date = new Date()
                def day = runDate == "" ? new SimpleDateFormat("dd-MM-yyyy").format(date) : runDate

                if (!fileExists("${resultPath}")) {
                    println("Testing script failed at: $specName ${BUILD_NUMBER}. Please fix this and re-run the test.")
                    def message = """|Failed automation script. Please fix and re-run this spec
                                           |Environment: ${envName}
                                           |Spec: ${specName}
                                           |Build time: ${day}
                                           |Reports are presented at:
                                           |https://jenkins-stg.allnowgroup.com/blue/organizations/jenkins/automation_frontend/detail/automation_frontend/${BUILD_NUMBER}/pipeline""".stripMargin()
                    googlechatnotification(
                            url: "https://chat.googleapis.com/v1/spaces/AAAAyBL5iPc/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=iiBA1479xJqFQ22XiIR8xwGsbRGmdXbQnvf9NGmsbTw%3D",
                            message: message,
                            sameThreadNotification: false
                    )
                } else {
                    container("cloud-sdk") {
                        def global = new Global()
                        global.googleCloudAuth()

                        def specInfo = sh(script: "echo ${specName}", returnStdout: true).tokenize("/")
                        def spec = specInfo[0]
                        def specFile = specInfo[specInfo.size() - 1].trim().toString()
                        def destination = "gs://automation-result/tmsbrain_automationfe/$day/$envName/$spec/$branchName"

                        // Remove old result file
                        ["passed", "failed"].each { status ->
                            try {
                                sh "gsutil rm $destination/$status/${specFile}.html"
                                sh "gsutil rm $destination/$status/${specFile}.mp4"
                            } catch (e) {
                                println(e)
                            }
                        }

                        sh "cp ${resultPath} ${specFile}.html"
                        sh "cp ${videosFolderPath}/${specFile}.mp4 ${specFile}.mp4"

                        sh "gsutil cp ${specFile}.html $destination/$testStatus/"
                        sh "gsutil cp ${specFile}.mp4 $destination/$testStatus/"
                    }
                }
            }
        }
    }
}
