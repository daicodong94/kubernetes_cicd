import tms.allnow.*

def call() {
    def global = new Global()
    def pipeline = new Pipeline()
    def jobParams = [
            choice(choices: ['dev', 'staging', 'preprod', 'prod'], name: 'runenv', description: 'Target environments'),
            string(defaultValue: 'master', name: 'branch', description: 'Target branch'),
            string(defaultValue: 'latest', name: 'tag', description: 'Image tag'),
            booleanParam(name: 'allowDeploy', description: 'Kubernetes auto deploy')
    ]

    pipeline.triggerByJob(jobParams)

    PodTemplates pt = new PodTemplates("all-now-tms-staging", "${runenv}").addMaven().addKaniko()
    podTemplate(yaml: pt.toString()) {
        env.GIT_URL = 'https://gitlab.com/asc-jiuye/backend/lbs.git'
        env.HARBOR_PROJECT = "all-now-tms-${runenv}/tms/lbs-main"
        env.DIR = 'lbs-main'

        global.defaultEnv("${runenv}")

        node(POD_LABEL) {
            def tag = sh(returnStdout: true, script: "echo ${params.tag}-${env.BUILD_NUMBER}").trim()

            stage("Pulling ${JOB_NAME} repository") {
                echo "mirror name: ${HARBOR_PROJECT}-${runenv}:${tag}"
                git branch: "${branch}", credentialsId: "${CREDENTIALSID}", url: "${GIT_URL}"
            }

            global.mavenBuild([
                    'mvn clean package -Dmaven.test.skip=true'
            ])

            def templateDir = global.getTemplate("${runenv}")

            stage('Build Image and Push') {
                dir("${DIR}") {
                    EXPOSE_LINE = sh(returnStdout: true, script: "awk '/EXPOSE/{print}' Dockerfile").trim()
                    sh "cp ${templateDir}/DockerTemplates/DubboDockerfile Dockerfile"
                    sh "cp ${templateDir}/DockerTemplates/entrypoint.sh ."
                    sh "sed -i 's/#{EXPOSE}/${EXPOSE_LINE}/g' Dockerfile"
                    sh "sed -i 's/#{SERVICES}/${DIR}/g' Dockerfile"

                    global.kanikoBuild(
                            HARBOR_DOMAIN: "$HARBOR_DOMAIN",
                            HARBOR_PROJECT: "$HARBOR_PROJECT",
                            runenv: "$runenv",
                            tag: tag,
                            context: "."
                    )
                }
            }

            if (params.allowDeploy) {
                def k8s = new K8s()
                k8s.deploy(
                        serviceName: "${JOB_NAME}",
                        imageName: "${JOB_NAME}-${runenv}",
                        baseDIR: "tms",
                        dockerTag: "${tag}",
                        envName: "${runenv}",
                        registryDocker: "asia.gcr.io",
                        projectID: "all-now-tms-${runenv}"
                )
            }
        }
    }
}
