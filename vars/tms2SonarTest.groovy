import tms.allnow.*
import java.text.SimpleDateFormat

def call() {
    def global = new Global()
    def pipeline = new Pipeline()
    def jobParams = [
            string(defaultValue: 'dev', name: 'branch', description: 'Target branch')
    ]

    pipeline.triggerByJob(jobParams)
    PodTemplates pt = new PodTemplates("all-now-tms-staging", "staging").addMaven()

    podTemplate(yaml: pt.toString()) {
        env.GIT_URL = 'https://gitlab.com/asc-jiuye/backend/tms2.git'

        global.defaultEnv("staging")

        node(POD_LABEL) {
            def tag = sh(returnStdout: true, script: "echo ${params.tag}-${env.BUILD_NUMBER}").trim()
            def date = new Date()
            def day = new SimpleDateFormat("dd-MM-yyyy").format(date)

            stage("Pulling ${JOB_NAME} repository") {
                echo "Pulling: ${GIT_URL} branch: ${branch}"
                git branch: "${branch}", credentialsId: "${CREDENTIALSID}", url: "${GIT_URL}"
            }

            global.mavenBuild([
                'mvn clean install -U'
            ])

            // Wait ArgoCD take actions
            // sleep(time: "10", unit: "HOURS")

            global.mavenBuild([
                  'mvn clean verify -P coverage sonar:sonar \
                   -Dsonar.projectKey=tms2 \
                   -Dsonar.host.url=http://sonarqube.allnow.tms.staging.internal \
                   -Dsonar.login=b0000fffeaa75648c37646c0901d9b138739fc2a'
            ])

            httpRequest(
                url: "https://discord.com/api/webhooks/1090843004958416947/v-7V9s-g1d01BznJDQMYkxnOkIPUvaCC-IXG5RV-_5bRKLqQhldn6C2vXBOVTcf3vbcL",
                httpMode: 'POST',
                contentType: 'APPLICATION_JSON',
                requestBody: """{
                    "content": "tms2-service sonarqube scan has finished.\\nRun date: $day\\nCheck the results at:\\nsonarqube server: http://sonarqube.allnow.tms.staging.internal/\\njenkins job: https://jenkins-stg.allnowgroup.com/view/automation/job/tms2-sonar-scan/${BUILD_NUMBER}/console"
                }"""
            )
        }
    }
}
