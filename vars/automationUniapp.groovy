import tms.allnow.*

import java.text.SimpleDateFormat

def call() {
    node {
        TimeZone.setDefault(TimeZone.getTimeZone('Asia/Bangkok'))

        def uniBranch = "TMSB-2927"
        def uniGitUrl = "https://gitlab.com/asc-jiuye/automate/tmsbrain_automationuniapp.git"
        def testName
        PodTemplates pt = new PodTemplates("all-now-tms-staging").addCloudsdk()

        node('allnow-tms-staging-mobile-server') {
            stage("Preparing tmsbrain_automationuniapp") {
                sh "rm -rf tmsbrain_automationuniapp* results"

                dir("tmsbrain_automationuniapp") {
                    git branch: uniBranch, credentialsId: "Gitlab_crt_staging", url: uniGitUrl
                    sh "docker cp ./app/AllNowDriver_Oct_26.apk android-12.0:/AllNowDriver_Oct_26.apk"

                    def testFileName = sh(script: "find ./Tests -type f -printf \"%f\\n\"", returnStdout: true).tokenize("\n")

                    testName = input(
                            message: "Please choose robot test file",
                            parameters: [
                                    choice(choices: testFileName, name: 'testName')
                            ]
                    )
                }
            }

            stage("Executing robot framework") {
                dir("tmsbrain_automationuniapp") {
                    try {
                        sh "robot -d results Tests/${testName}"
                    } catch (e) {
                        println(e)
                    }
                    sh "zip -r results.zip results"
                    archiveArtifacts(artifacts: "results.zip", onlyIfSuccessful: true)
                }
            }
        }

        podTemplate(yaml: pt.toString()) {
            node(POD_LABEL) {
                def date = new Date()
                def day = new SimpleDateFormat("dd-MM-yyyy").format(date)
                def testNamePrefix = testName.split("\\.")[0]

                stage("Uploading results") {
                    container("cloud-sdk") {
                        def global = new Global()
                        global.googleCloudAuth()

                        def googleStorageDestinatioin = "gs://automation-result/tmsbrain_automationuniapp/$day/${testNamePrefix}"

                        unarchive mapping: ["results.zip": '.']
//                    Upload results
                        sh "gsutil cp results.zip ${googleStorageDestinatioin}/"
                    }
                }

                stage("Send google chat notification") {
                    googlechatnotification(
                            url: "https://chat.googleapis.com/v1/spaces/AAAAyBL5iPc/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=iiBA1479xJqFQ22XiIR8xwGsbRGmdXbQnvf9NGmsbTw%3D",
                            message: """|Automation Uniapp test has finished
                                        |Run date: ${day}
                                        |Test name: ${testName}
                                        |Check the results at:
                                        |https://console.cloud.google.com/storage/browser/automation-result/tmsbrain_automationuniapp/${day}/${testNamePrefix}?project=all-now-tms-staging""".stripMargin(),
                            sameThreadNotification: false
                    )
                }
            }
        }
    }
}
