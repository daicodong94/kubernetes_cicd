import tms.allnow.*

def call() {
    def global = new Global()
    def pipeline = new Pipeline()
    def jobParams = [
            choice(choices: ['dev', 'staging', 'preprod', 'prod'], name: 'runenv', description: 'Target environments'),
            string(defaultValue: 'master', name: 'branch', description: 'Target branch'),
            string(defaultValue: 'latest', name: 'tag', description: 'Image tag'),
            booleanParam(name: 'allowDeploy', description: 'Kubernetes auto deploy')
    ]

    pipeline.triggerByJob(jobParams)

    PodTemplates pt = new PodTemplates("all-now-tms-staging","${runenv}").addMaven("3.8.5-openjdk-17").addKaniko()
    podTemplate(yaml: pt.toString()) {
        env.GIT_URL = 'https://gitlab.com/asc-jiuye/backend/open-api.git'
        env.HARBOR_PROJECT = "all-now-tms-${runenv}/base/open-api"
        env.DIR = 'open-api'

        global.defaultEnv("${runenv}")

        node(POD_LABEL) {
            def tag = sh(returnStdout: true, script: "echo ${params.tag}-${env.BUILD_NUMBER}").trim()

            stage("Pulling ${JOB_NAME} repository") {
                git branch: branch, credentialsId: "${CREDENTIALSID}", url: "${GIT_URL}"
            }

            global.mavenBuild([
                    'mvn clean package -Dmaven.test.skip=true'
            ])

            stage('Build Image and Push') {
                global.kanikoBuild(
                        HARBOR_DOMAIN: "$HARBOR_DOMAIN",
                        HARBOR_PROJECT: "$HARBOR_PROJECT",
                        runenv: "$runenv",
                        tag: tag,
                        context: "."
                )
            }

            if (params.allowDeploy) {
                def k8s = new K8s()
                k8s.deploy(
                        serviceName: "${JOB_NAME}",
                        imageName: "${JOB_NAME}-${runenv}",
                        baseDIR: "base",
                        dockerTag: "${tag}",
                        envName: "${runenv}",
                        registryDocker: "asia.gcr.io",
                        projectID: "all-now-tms-${runenv}"
                )
            }
        }
    }
}
