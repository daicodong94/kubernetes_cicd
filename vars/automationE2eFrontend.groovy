import tms.allnow.*
import java.text.SimpleDateFormat

def call() {
    TimeZone.setDefault(TimeZone.getTimeZone('Asia/Bangkok'))

    def pipeline = new Pipeline()
    def jobParams = [
            string(defaultValue: 'None', name: 'envName', trim: true, description: 'staging or production'),
            string(defaultValue: 'develop', name: 'branchName', trim: true, description: 'default: develop')
    ]
    pipeline.triggerByJob(jobParams, true)
    def envName = params.envName
    def branchName = params.branchName

    if (envName == "None") {
        error("Please select environment")
        return
    }

    def testAutomation = new TestAutomation()

    PodTemplates pt = new PodTemplates("all-now-tms-staging")
    podTemplate(yaml: pt.toString()) {
        node(POD_LABEL) {
            def specsList
            def date = new Date()
            def day = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss").format(date)
            def specsE2eList = []

            stage("Get specs list") {
                dir("tmsbrain_automationfe") {
                    git branch: "${branchName}", credentialsId: "Gitlab_crt_staging", url: "https://gitlab.com/asc-jiuye/automate/tmsbrain_automationfe.git"

                    dir("cypress/e2e/specs/E2E") {
                        specsList = sh(script: "find * -type f", returnStdout: true).tokenize("\n")
                        specsList.each { spec ->
                            specsE2eList.add("E2E/${spec}")
                        }
                    }
                }
            }

            stage("Executing automation") {
                testAutomation.runSpec(specsList: specsE2eList,envName: envName, runDate: day, branchName: branchName)
            }

            stage("Send google chat notification") {
                googlechatnotification(
                        url: "https://chat.googleapis.com/v1/spaces/AAAAyBL5iPc/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=iiBA1479xJqFQ22XiIR8xwGsbRGmdXbQnvf9NGmsbTw%3D",
                        message: """|E2E Automation test has finished
                                    |Run date: $day
                                    |Environment: ${envName}
                                    |Branh: ${branchName}
                                    |Check the results at:
                                    |https://console.cloud.google.com/storage/browser/automation-result/tmsbrain_automationfe/${day}/${envName}?project=all-now-tms-staging""".stripMargin(),
                        sameThreadNotification: false
                )
            }
        }
    }
}
