import tms.allnow.*

def call() {
    def global = new Global()
    def pipeline = new Pipeline()
    def jobParams = [
            choice(choices: ['dev', 'staging', 'preprod', 'prod'], name: 'runenv', description: 'Target environments'),
            string(defaultValue: 'master', name: 'branch', description: 'Target branch')
    ]

    pipeline.triggerByJob(jobParams)

    PodTemplates pt = new PodTemplates("all-now-tms-staging", "${runenv}").addMaven()
    podTemplate(yaml: pt.toString()) {
        if ("${runenv}" == "staging" || "${runenv}" == "dev") {
            error("Trace-support not exist on Staging")
        }
        if ("${runenv}" == "prod") {
            env.GIT_URL = 'https://gitlab-dev.allnowgroup.com/base/trace-support.git'
        }

        global.defaultEnv("${runenv}")

        node(POD_LABEL) {
            stage("Pulling ${JOB_NAME} repository") {
                git branch: "${branch}", credentialsId: "${CREDENTIALSID}", url: "${GIT_URL}"
            }

            global.mavenBuild([
                    'mvn clean deploy -Dmaven.test.skip=true'
            ])
        }
    }
}
