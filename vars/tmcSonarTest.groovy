import tms.allnow.*
import java.text.SimpleDateFormat

def call() {
    def global = new Global()
    def pipeline = new Pipeline()
    def jobParams = [
            string(defaultValue: 'dev', name: 'branch', description: 'Target branch')
    ]

    pipeline.triggerByJob(jobParams)
    PodTemplates pt = new PodTemplates("all-now-tms-staging", "staging").addMaven()

    podTemplate(yaml: pt.toString()) {
        env.GIT_URL = 'https://gitlab.com/asc-jiuye/backend/tmc.git'

        global.defaultEnv("staging")

        node(POD_LABEL) {
            def tag = sh(returnStdout: true, script: "echo ${params.tag}-${env.BUILD_NUMBER}").trim()
            def date = new Date()
            def day = new SimpleDateFormat("dd-MM-yyyy").format(date)

            stage("Pulling ${JOB_NAME} repository") {
                echo "Pulling: ${GIT_URL} branch: ${branch}"
                git branch: "${branch}", credentialsId: "${CREDENTIALSID}", url: "${GIT_URL}"
            }

            global.mavenBuild([
                'mvn clean install -U'
            ])

            // Wait ArgoCD take actions
            // sleep(time: "10", unit: "HOURS")

            global.mavenBuild([
                  'mvn clean verify -P coverage sonar:sonar \
                   -Dsonar.projectKey=tmc \
                   -Dsonar.host.url=http://sonarqube.allnow.tms.staging.internal \
                   -Dsonar.login=7d0e2da686dbee320b3dc2390e66ebf54d90b32f'
            ])

            httpRequest(
                url: "https://discord.com/api/webhooks/1090843004958416947/v-7V9s-g1d01BznJDQMYkxnOkIPUvaCC-IXG5RV-_5bRKLqQhldn6C2vXBOVTcf3vbcL",
                httpMode: 'POST',
                contentType: 'APPLICATION_JSON',
                requestBody: """{
                    "content": "tmc-service sonarqube scan has finished.\\nRun date: $day\\nCheck the results at:\\nsonarqube server: http://sonarqube.allnow.tms.staging.internal/\\njenkins job: https://jenkins-stg.allnowgroup.com/view/automation/job/tmc-sonar-scan/${BUILD_NUMBER}/console"
                }"""
            )
        }
    }
}
